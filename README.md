# Deep Learning based search for pre-microRNAs in SARS-CoV-2 genome

The pipeline presented here is highly experimental and should not be considered as finalized research.
New items and results will be posted regularly as the research on SARS-CoV-2 microRNA identification 
is evolving rapidly. It should be noted that SARS-CoV-2 is a single-stranded RNA virus, much like HIV and
influenza among others.

Why microRNAs? microRNAs are a subfamily of small non-coding RNAs (~18-23 nucleotides) that regulate 
gene expression through mRNA degradation and/or translation suppression. Since their initial discovery 
in 1993 ([Lee RC et al, Cell, 1993](https://linkinghub.elsevier.com/retrieve/pii/0092-8674(93)90529-Y)),
thousands of studies have associated microRNAs with a plethora of physiological and pathological conditions.

The miRNA biogenesis begins in the nucleus (Figure 1). MicroRNA genes are typically transcribed from the Polymerase II 
complex releasing primary microRNA transcripts containing a hairpin like structure, which is the precursor 
microRNA. This is cleaved by the Drosha/DGCR8 complex releasing the hairpin in the nucleus. Pre-microRNAs 
are exported in the cytoplasm by Exportin-5, where it is processed by the endonuclease Dicer into the mature 
microRNA form.

![MicroRNA_biogenesis_pathway](/source/images/mirna_biogenesis.png "MicroRNA biogenesis pathway")*Figure 1. MicroRNA biogenesis pathway.*

Viruses can be thought of as intracellular parasites that require the cell's resources to replicate 
([Aguado LC et al, PLOS PATHOGENS, 2018](https://journals.plos.org/plospathogens/article?id=10.1371/journal.ppat.1006963)). 
Most of the time, this replication is achieved by reducing the cell defenses using virus-encoded proteins and microRNAs. 
For example, the Herpesviridae family (DNA viruses) encodes up to fourteen different microRNAs. Their function is two-fold: 
a) they regulate the abundance of viral proteins directly and b) they act as mimics for host encoded microRNAs targeting 
host transcripts resulting in an environment that favors replication and/or latency.

On the other hand, RNA viruses didn't show any microRNA encoding evidence for a long time, and this was hypothesized to be 
due to three reasons: a) the microRNA biogenesis machinery (Drosha/Dgcr8) is inaccessible in the cytoplasm where most RNA 
viruses replicate, b) pre-microRNA extraction from the virus's transcript would lead to its degradation and c) the mature 
microRNA might target and degrade the virus’s own antigenome.

There is however increasing evidence in the literature that RNA viruses, such as Ebola, also encode microRNAs either 
by gaining access to the host's microRNA biogenesis machinery or by non-canonical microRNA biogenesis pathways 
([Duy Janice et al, Scientific Reports, 2018](https://www.nature.com/articles/s41598-018-23916-z)).

## Data and software walkthrough 

The SARS-CoV-2 genome (LC528232) was downloaded from [NCBI](https://www.ncbi.nlm.nih.gov/nuccore/LC528232) and is located 
in `Data/SARS-CoV-2` along with the encoded genes (as listed in NCBI) and several other help files.

The pre-microRNA prediction algorithm that was used is [MuStARD](https://gitlab.com/RBP_Bioinformatics/mustard), which is 
a Deep Learning framework that was developed in Dr Panagiotis Alexiou 
[research group](https://www.ceitec.eu/panagiotis-alexiou-ph-d/u91502) at 
[Central European Institute for Technology](https://www.ceitec.eu/), Masaryk University, Czech Republic.

MuStARD is currently under peer review. The preprint can be found [here](https://www.biorxiv.org/content/10.1101/547679v2).

Before we begin, do the following (all results are already available in the repository's subdirectories):

1. Clone MuStARD repository and install all dependencies.

1. Clone this repository into the same directory that MuStARD was cloned.

1. Download [IGV](https://software.broadinstitute.org/software/igv/) genome browser for viewing the results.

## Scan SARS-CoV-2 genome with MuStARD

**Whole genome scan with MuStARD**

Go to MuStARD's directory and execute the following command:

`./MuStARD.pl predict --winSize 100 --staticPredFlag 0 --step 1 --modelDirName mirSF --inputMode sequence,RNAfold --chromList all --targetIntervals ../sars-cov-2_mirsearch/Data/genome_table.bed --genome ../sars-cov-2_mirsearch/Data/genome.fa --consDir NA --dir ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2 --model ../sars-cov-2_mirsearch/Pretrained_models/MuStARD-mirSF/CNNonRaw.hdf5 --modelType CNN --classNum 2`

The output of the command can already be found in `sars-cov-2_mirsearch/Jobs/SARS-Cov-2`.

**Get the locations for the predicted pre-miRNAs**

`perl src/utilities/perl/call_hotspots_per_threshold.pl --minThresh 0.1 --maxThresh 0.98 --threshStep 0.02 --winSize 100 --predFile ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/bedGraph_tracks/predictions.chrLC528232.1.class_0.bedGraph.gz --outDir ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/hotspots_per_threshold`

Since both strands of the viral genome were scanned, predictions for both stranded and unstraded strategies will be placed in `sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/hotspots_per_threshold`.

**Get secondary structure for predicted pre-miRNAs (score cutoff at 0.8)**

Create theshold specific directories:

`mkdir ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/RNAfold && ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/RNAfold/stranded.threshold.0.8`

Get predicted pre-miRNA sequences:

`bedtools getfasta -s -name -fi ../sars-cov-2_mirsearch/Data/genome.fa -bed ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/hotspots_per_threshold/hotspots.stranded.summits.threshold.0.8.bed -fo ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/RNAfold/stranded.threshold.0.8/0.8.fa`

Get the secondary structure:

`cd ../sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/RNAfold/stranded.threshold.0.8 && RNAfold --jobs=2 --infile=0.8.fa --outfile=0.8.RNAfold`

Results can be quickly accessed in `sars-cov-2_mirsearch/Results`

The secondary structure of hsa-mir-21, a member of the most famous family of microRNAs, is shown in Figure 2 to provide an idea about how real pre-miRNAs fold.

![mir21_structure](/source/images/hsa-mir-21_ss.png "mir21 structure")*Figure 2. Hsa-mir-21 secondary structure (MFE -36.04 kcal/mol).*

The secondary structures of the top 4 predicted pre-miRNAs are shown in the following figures.

#1 predicted pre-miRNA (0.991 MuStARD score, MFE -20.61 kcal/mol).

![predicted_structure1](/source/images/prediction_chrLC528232.1_-_8_ss.png "Predicted structure #1")

#2 predicted pre-miRNA (0.979 MuStARD score, MFE -35.42 kcal/mol).

![predicted_structure2](/source/images/prediction_chrLC528232.1_+_8_ss.png "Predicted structure #2")

#3 predicted pre-miRNA (0.968 MuStARD score, MFE -19.62 kcal/mol).

![predicted_structure3](/source/images/prediction_chrLC528232.1_-_14_ss.png "Predicted structure #3")

#4 predicted pre-miRNA (0.967 MuStARD score, MFE -36.32 kcal/mol).

![predicted_structure4](/source/images/prediction_chrLC528232.1_+_6_ss.png "Predicted structure #4")

**View results on IGV**

Load IGV and open the following files:

1. `sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/bedGraph_tracks/predictions.chrLC528232.1.class_0.bedGraph.gz`

1. `sars-cov-2_mirsearch/Data/SARS-CoV-2/genes.bed`

1. `sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/hotspots_per_threshold/hotspots.stranded.threshold.0.9.bed`

1. `sars-cov-2_mirsearch/Jobs/SARS-Cov-2/predict/scan/mirSF/hotspots_per_threshold/hotspots.stranded.threshold.0.9.top4.bed`

IGV should show the following tracks (Figure 7).

![IGV_stranded_threshold_0.9](/source/images/stranded.threshold.0.9.png "IGV stranded threshold 0.9")*Figure 7. IGV for stranded pre-miRNA predictions with MuStARD score threshold at 0.9. Top track depicts the raw prediction signal. Directly below the predictions that have a score over 0.8 are depicted. The 3rd track shows the top 4 predictions and the annotated SARS-CoV-2 genes are at the bottom track.*
